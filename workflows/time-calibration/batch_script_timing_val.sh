#!/bin/bash

set -x

CLUSTERID=${1}
JOBID=${2}
INFILE=${3}
TASK=${4}

EOSDIR=${5}     # base directory for dat, sqlite output 
WDIR=${6}       # CMSSW_BASE directory
EOSPLOTS=${7}   # plot output directory
PLOTSURL=${8}   # plot output for output field

RUNS=${9}       # list of runs
FILL=${10}      # fill number for the runs
ISRERECO=${11}
ISCC=${12}
TAG=${13}
GT=${14}

trap 'echo "Kill signal received"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM

export HOME=/afs/cern.ch/user/e/ecalgit/
export EOSCMS_MGM_URL=root://eoscms.cern.ch
export EOSUSER_MGM_URL=root://eosuser.cern.ch

source /cvmfs/cms.cern.ch/cmsset_default.sh
cd $WDIR
eval $(scram runtime -sh)
cd -

ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"

# local output directory
mkdir output
RETPROD=$?

# save payload (for reco)
if [ "$ISRERECO" != "True" ]
then
    for run_number in ${RUNS//,/ }
    do
        RUN="$run_number"            # get master run
    done 
    bash savePayload.sh $RUN $GT $ISCC output/
    RETPROD=$((RETPROD+$?))

    OUTDIR=$EOSDIR/validation-reco/$FILL/
else
    OUTDIR=$EOSDIR/validation-rereco/$FILL/
fi    

# Produce dat files
RunTimeAverage --files=$INFILE --nSigma=2 --maxRange=10 --ebECut=0.5 --eeECut=0.5 --outputCalibCorr=ecalTiming-corr_$FILL.dat --outputFile=ecalTiming_$FILL.root --outputDir=output/
RETPROD=$((RETPROD+$?))

# Plot from the dat file
python3 plot_calibration.py output/ecalTiming-corr_$FILL.dat $ISRERECO output/
RETPROD=$((RETPROD+$?))

# Produce sqlite (for reco)
if [ "$ISRERECO" != "True" ]
then
    SQLITE=$OUTDIR/ecalTiming-abs_$FILL.db
    bash produceSqlite.sh $FILL $TAG output/
    RETPROD=$((RETPROD+$?))
fi

# Remove stuff that will no longer be used
rm output/ecalTiming.dat
rm output/ecalTiming_$FILL.root

# Copy output to EOS
RETCOPY=0
if [ "$RETPROD" == "0" ]
then
    # Copy .dat file to EOS
    eos $EOSCMS_MGM_URL mkdir -p $OUTDIR
    xrdcp -f output/ecalTiming-corr_$FILL.dat $EOSCMS_MGM_URL/$OUTDIR
    RETCOPY=$((RETCOPY+$?))

    # Copy plots to EOS plots directory
    EOSPLOTS=$EOSPLOTS/$FILL
    eos $EOSUSER_MGM_URL mkdir -p $EOSPLOTS
    LIST=$(ls output/ecalTiming*png)
    for file in ${LIST}; do
        eos $EOSUSER_MGM_URL cp -p -n ${file} $EOSUSER_MGM_URL/$EOSPLOTS/$(basename ${file})
        RETCOPY=$((RETCOPY+$?))
    done

    # Copy sqlite file
    if [ "$ISRERECO" != "True" ]
    then
        xrdcp -f output/ecalTiming-abs_$FILL.db $EOSCMS_MGM_URL/$SQLITE
        RETCOPY=$((RETCOPY+$?))

        # Create symbolic links to sqlite file for rereco
        for run_number in ${RUNS//,/ }
        do
            DESTDIR=$EOSDIR/rereco/$run_number/
            eos $EOSCMS_MGM_URL mkdir -p $DESTDIR
            ln -sf $SQLITE $DESTDIR/ecalTiming-abs.db
            RETCOPY=$((RETCOPY+$?))
        done
    fi
fi

RET=$(echo "$RETPROD+$RETCOPY" | bc)

if [ "$RET" == "0" ]
then
    OUTNAME=$OUTDIR/ecalTiming-corr_$FILL.dat
    PLOTSURL=$PLOTSURL/$FILL/

    if [ "$ISRERECO" != "True" ]
    then
        ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${OUTNAME}" "plots:${PLOTSURL}" "sqlite:${SQLITE}"
    else
        ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${OUTNAME}" "plots:${PLOTSURL}"
    fi
else
    ecalautomation.py $TASK jobctrl --id $JOBID --failed
fi

exit $RET
