#!/bin/bash

set -x

CLUSTERID=${1}
JOBID=${2}
INFILE=${3}
TASK=${4}
EOSDIR=${5}
WDIR=${6}
GT=${7}

trap 'echo "Kill signal received"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM

export HOME=/afs/cern.ch/user/e/ecalgit/
export EOS_MGM_URL=root://eoscms.cern.ch

source /cvmfs/cms.cern.ch/cmsset_default.sh
cd $WDIR
eval $(scram runtime -sh)
cd -

ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"

mkdir output

#copy alignment tags to the condor machine
RUN=`echo $TASK | sed -e 's/^.*run_number://g' | sed -e 's/,.*$//g'`
ALIGNMENTDIR=$(dirname "$(dirname "$EOSDIR")")
xrdcp -f $EOS_MGM_URL/$ALIGNMENTDIR/align/$RUN/EBAlign_test.db $PWD/
xrdcp -f $EOS_MGM_URL/$ALIGNMENTDIR/align/$RUN/EEAlign_test.db $PWD/

export PYTHON3PATH=$PYTHON3PATH:$CMSSW_BASE/src/EcalValidation/EcalAlignment/test
cmsDriver.py reco -s RAW2DIGI,L1Reco,RECO,PAT --eventcontent "" --conditions $GT --era Run3 --data --python_filename=align_rereco_cfg.py --no_exec --processName=EcalAlignment --customise align_rereco_customise.py -n -1 --filein=$INFILE

cmsRun align_rereco_cfg.py
RETCMSSW=$?

if [ "$RETCMSSW" == "0" ]
then
    # move the file to the final location
    eos mkdir -p $EOSDIR
    xrdcp -f output.root $EOS_MGM_URL/$EOSDIR/ecal_alignment_rereco_$JOBID.root
    RETCOPY=$?
    rm EBAlign_test.db EEAlign_test.db
    OFILE=$EOSDIR/ecal_alignment_rereco_$JOBID.root
else
    RETCOPY=1
fi

RET=$(echo "$RETCMSSW+$RETCOPY" | bc)

if [ "$RET" == "0" ]
then
    ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${OFILE}"
else
    ecalautomation.py $TASK jobctrl --id $JOBID --failed
fi

exit $RET
